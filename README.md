# Levit's Django CookieCutter template for reusable apps

### Before getting started

This cookie cutter provides a reuasable app with a sample Django project built according to the _project app_ architecture.

In short, with the _project app_ architecture, the almost empty directory named after your project becomes an *app* like any other app in your project. This has numerous advantages, the most noticeable of them being that you don't need to create a _core_ or _base_ app but can use your project app instead.

If you haven't seen [Anatomy of a Django Project by Mark Lavin](https://www.youtube.com/watch?v=ajEDo1semzs), I would emcourage you to do so to get a better grasp of what the _project app_ architecture is about.

### CookieCutter

For more info on CookieCutter, please visit [their documentation](http://cookiecutter.readthedocs.org/en/latest/)

### Requirements

- Latest [Python & virtualenv](https://www.python.org/downloads/release) - on Debian/Ubuntu, make sure to install the system `python-virtualenv` as well
- Latest [CookieCutter](http://cookiecutter.readthedocs.org/en/latest/) - (`sudo pip install -U cookiecutter` to upgrade)

### Usage

To use, simply run 
`cookiecutter https://bitbucket.org/levit_scs/cc_reusable_app.git`

This project is licensed under the [MIT License](http://opensource.org/licenses/MIT)
