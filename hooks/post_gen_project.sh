#!/bin/bash
USER="{{ cookiecutter.username }}"
EMAIL="{{ cookiecutter.email }}"
if [ `which virtualenv|grep virtualenv 2>/dev/null` ]
then
  virtualenv -p {{ cookiecutter.python_executable }} venv
else
  {{cookiecutter.python_executable}} -m venv venv
fi
source venv/bin/activate
echo "Installing backend dependencies"
pip install --upgrade pip setuptools
pip install -r requirements.txt
pip freeze --local > requirements.txt
cp environ.py.dist environ.py
python manage.py migrate --noinput
{% if cookiecutter.create_superuser == 'yes' %}
  echo "Creating superuper {{ cookiecutter.username }}"
  python manage.py createsuperuser --username {{ cookiecutter.username }} --email {{ cookiecutter.email }}
{% endif %}
mv gitignore.txt .gitignore
git init .
git add .
git commit -m "{{ cookiecutter.project_name }} initialized with cookiecutter from http://bitbucket.org/levit_scs/cc_reusable_app"
